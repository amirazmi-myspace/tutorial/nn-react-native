import React, { useState } from 'react';
import { FlatList, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default function App() {
  const [people, setPeople] = useState([
    { name: 'shaun', id: '1' },
    { name: 'yoshi', id: '2' },
    { name: 'mario', id: '3' },
    { name: 'luigi', id: '4' },
    { name: 'peach', id: '5' },
    { name: 'toad', id: '6' },
    { name: 'bowser', id: '7' },
    { name: 'rimau', id: '8' },
  ])

  const pressHandler = (id) => setPeople((prevPeople) => prevPeople.filter((person) => person.id !== id))

  const renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => pressHandler(item.id)}>
      <Text style={styles.item}>{item.name}</Text>
    </TouchableOpacity>
  )

  return (
    <View style={styles.container}>

      <FlatList data={people} numColumns={2} keyExtractor={(item) => item.id} renderItem={renderItem} />

      {/* <ScrollView>
        {people.map((person) =>
          <View key={person.key}>
            <Text style={styles.item}>{person.name}</Text>
          </View>
        )}
      </ScrollView> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 40,
    paddingHorizontal: 20
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  item: {
    marginTop: 24,
    padding: 30,
    backgroundColor: 'pink',
    fontSize: 24,
    marginHorizontal: 10,
    marginTop: 24
  }
});
